
#ifndef _DELAUNAY_H
#define _DELAUNAY_H

#define NUM_P 15000
#define SIZE 100

#define MIN(a,b) (a < b) ? a : b;
#define MAX(a,b) (a > b) ? a : b;

typedef struct
{
	double x;
	double y;
} Point2;

typedef struct
{
	double x;
	double y;
} Point2D;

typedef struct
{
	int v0;
	int v1;
} Edge2;

typedef struct
{
	unsigned int v0;
	unsigned int v1;
} Edge2D;

typedef struct _triangle
{
	int v0;
	int v1;
	int v2;
	
	Point2 center;
	double sqr_radius;
	
	char complete;
	struct _triangle *prev;
	struct _triangle *next;
} Triangle2;

typedef struct
{
	Point2 center;
	double sqr_radius;
	unsigned int v0;
	unsigned int v1;
	unsigned int v2;
	int complete;
} Triangle2D;

typedef struct
{
	unsigned int length;
	unsigned int firstZero;
	unsigned int firstOne;
	int firstMinusOne;
	Triangle2D* array;
} Array;

typedef struct
{
	Point2D min;
	Point2D max;
} BoundingBox2D;

typedef struct
{
	unsigned int length;
	Edge2D* array;
} EdgeArray;

int main(int argc, char *argv[]);

Point2 init_point(double x, double y);
Point2D init_point2d(double x, double y);
Triangle2* create_triangle( int v0, int v1, int v2,
													 Triangle2* prev, Triangle2* next,
													 Point2* points );
void set_triangle2d(int v0, int v1, int v2, int complete, Point2D* points, Triangle2D* tri);

Triangle2* delaunay(Point2* points);
void delaunay2d(Point2D* points, Array* arr);
void export_delaunay(Triangle2* triangulation, Point2* points);
void export_delaunay2d(Array arr, Point2D* points);

void quicksortX_recursive(int start, int end, Point2* points);
int partitionX(int start, int end, int pivot, Point2* points);
void swap(int left, int right, Point2* points);
void quicksortX_recursive2d(int start, int end, Point2D* points);
int partitionX2d(int start, int end, int pivot, Point2D* points);
void swap2d(int left, int right, Point2D* points);

#endif
