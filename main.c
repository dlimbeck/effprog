
#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <string.h>

int main(int argc, char *argv[])
{
	if ( argc == 2 )
		srand(atoi(argv[1]));
	else
		srand(time(NULL));
	int i;
#ifdef ARRAY
	Point2D* points = malloc((NUM_P+3)*sizeof(Point2D));
	for(int i = 0; i < NUM_P; ++i)
	{
		points[i].x = (double)(rand() % (1000*SIZE)) / 1000.0;
		points[i].y = (double)(rand() % (1000*SIZE)) / 1000.0;
	}
	// sort w.r.t. x-coordinate
	quicksortX_recursive2d(0, NUM_P-1, points); //necessary for algorithm
	
	for( i=0; i < NUM_P; ++i )
		printf("Point %d: %f, %f\n", i, points[i].x, points[i].y );
	
	Array arr;
	arr.length = NUM_P * 2 + 1;
	arr.firstZero = 0;
	arr.firstOne = 0;
	arr.firstMinusOne = 1;
	arr.array = malloc((NUM_P*2+1)*sizeof(Triangle2D));
	//Triangle2D triangles[] = malloc((NUM_P*2+1)*sizeof(Triangle2D));
	if(arr.array == NULL)
	{
		printf("Could not allocate enough memory for all Triangles\n");
		assert(0);
	}
	Triangle2D t2d = { .center = {0.0, 0.0}, .sqr_radius = 0.0, .v0 = 0, .v1 = 0, .v2 = 0, .complete = -1};
	//memcpy(&arr.array[0], &t2d, sizeof(Triangle2D));
	//t2d.complete = -1;
	for(unsigned int i = 1; i < 2*NUM_P+1; ++i)
	{
		memcpy(&arr.array[i], &t2d, sizeof(Triangle2D));
	}
	
	delaunay2d(points, &arr);
	export_delaunay2d( arr, points );
#else
	
	Point2* points = malloc((NUM_P+3)*sizeof(Point2));
	for ( i=0; i < NUM_P; ++i )
	{
		points[i].x = (double)(rand() % (1000*SIZE)) / 1000.;
		points[i].y = (double)(rand() % (1000*SIZE)) / 1000.;
		
		//printf("Point %d: %f, %f\n", i, points[i].x, points[i].y );
	}
	// sort w.r.t. x-coordinate
	quicksortX_recursive(0, NUM_P-1, points); //necessary for algorithm
	
	for( i=0; i < NUM_P; ++i )
		printf("Point %d: %f, %f\n", i, points[i].x, points[i].y );
	
	Triangle2* triangulation = delaunay(points);
	export_delaunay( triangulation, points );
#endif
	
	
	return 0;
}
