
#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

/* utility function */
Point2 init_point(double x, double y)
{
	Point2 p;
	p.x = x;
	p.y = y;
	
	return p;
}

Point2D init_point2d(double x, double y)
{
	Point2D p;
	p.x = x;
	p.y = y;
	return p;
}

void set_triangle2d(int v0, int v1, int v2, int complete, Point2D* points, Triangle2D* tri)
{
	tri->v0 = v0;
	tri->v1 = v1;
	tri->v2 = v2;
	
	tri->complete = complete;
	
	Point2D A = points[v0];
	double A2 = A.x*A.x + A.y*A.y;
	Point2D B = points[v1];
	double B2 = B.x * B.x + B.y * B.y;
	Point2D C = points[v2];
	double C2 = C.x * C.x + C.y * C.y;
	
	double D2 = 2 * (A.x * (B.y - C.y) + B.x * (C.y - A.y) + C.x * (A.y - B.y));
	
	tri->center.x = (A2 * (B.y - C.y) + B2 * (C.y - A.y) + C2 * (A.y - B.y)) / D2;
	tri->center.y = (A2 * (C.x - B.x) + B2 * (A.x - C.x) + C2 * (B.x - A.x)) / D2;
	
	tri->sqr_radius = (points[v0].x - tri->center.x) * (points[v0].x - tri->center.x)
						+ (points[v0].y - tri->center.y) * (points[v0].y - tri->center.y);
						
	Point2D dir0 = init_point2d(C.x - A.x, C.y - A.y);
	Point2D dir1 = init_point2d(C.x - B.x, C.y - B.y);
	double det = dir0.x * dir1.y - dir1.x * dir0.y;
	if(fabs(det) < 0.000001)
		printf("\tDet = %f\n", det);
}

/* CREATE_TRIANGLE
 * initialize everything you need
 */
Triangle2* create_triangle( int v0, int v1, int v2,
													 Triangle2* prev, Triangle2* next,
													 Point2* points )
{	
	Triangle2* tri = (Triangle2 *)malloc(sizeof(Triangle2));
	
	/* indices of points that form triangle */
	tri->v0 = v0;
	tri->v1 = v1;
	tri->v2 = v2;
	
	/* flag for algorithm
	 * -1 .. shall be deleted
	 *  0 .. not treated yet
	 *  1 .. already treated
	 */
	tri->complete = 0;
	
	/* compute circumcenter of the triangle */
	Point2 A = points[v0];
	double A2 = A.x*A.x + A.y*A.y;
	Point2 B = points[v1];
	double B2 = B.x*B.x + B.y*B.y;
	Point2 C = points[v2];
	double C2 = C.x*C.x + C.y*C.y;
	
	double D2 = 2*( A.x*(B.y-C.y) + B.x*(C.y-A.y) + C.x*(A.y-B.y) );
	
	tri->center.x = ( A2*(B.y-C.y) + B2*(C.y-A.y) + C2*(A.y-B.y) ) / D2;
	tri->center.y = ( A2*(C.x-B.x) + B2*(A.x-C.x) + C2*(B.x-A.x) ) / D2;
	
	/* compute squared radius of circumcircle */
	tri->sqr_radius =
			(points[v0].x - tri->center.x)*(points[v0].x - tri->center.x)
		+ (points[v0].y - tri->center.y)*(points[v0].y - tri->center.y);
							
	/* set previous and next triangle in triangulation list */
	tri->next = next;
	tri->prev = prev;
	
	/* TODO */
	Point2 dir0 = init_point(C.x - A.x, C.y - A.y);
	Point2 dir1 = init_point(C.x - B.x, C.y - B.y);
	double det = dir0.x * dir1.y - dir1.x * dir0.y;
	if ( fabs(det) < 0.000001 )
		printf("\tDet = %f\n", det);
	
	
	return tri;
}

Triangle2* delaunay(Point2* points)
{
	/* find bounding box of the point cloud */
	double min_x = points[0].x;
	double max_x = points[0].x;
	double min_y = points[0].y;
	double max_y = points[0].y;
	
	Point2 current;
	int i;
	for( i=1; i < NUM_P; ++i )
	{
		current = points[i];
		
		min_x = fmin(min_x, current.x);
		max_x = fmax(max_x, current.x);
		min_y = fmin(min_y, current.y);
		max_y = fmax(max_y, current.y);
	}
	
	/* create initial triangulation:
	 * compute a super-triangle that contains all points
	 * (it will be deleted in the end)
	 */
	double x_c = (min_x + max_x) / 2.;
	double y_c = (min_y + max_y) / 2.;
	double d_max = 3*fmax(max_x-min_x,max_y-min_y);
	Point2 p0 = init_point( x_c - 1*d_max, y_c - 0.75*d_max );
	Point2 p1 = init_point( x_c + 1*d_max, y_c - 0.75*d_max );
	Point2 p2 = init_point( x_c , y_c + 1.25*d_max );
	
	/* points for super-triangle are appended to point cloud list */
	points[NUM_P+0] = p0;
	points[NUM_P+1] = p1;
	points[NUM_P+2] = p2;
	
	/* super-triangle starts the triangulation */
	Triangle2* triangulation =  create_triangle( NUM_P+0, NUM_P+1, NUM_P+2,
																		 NULL, NULL, points );

	/* some variables */
	Triangle2* d_tri;
	double Dx2;
	double Dst2;
	Triangle2* tmp_tri;
	unsigned int num_tmp_tri;
	
	/* iterate over all points as one point at a time is inserted
	 * into the delaunay-triangulation such that it is a 
	 * delaunay-triangulation again
	 */
	int skip_point;
	for( i=0; i < NUM_P; ++i )
	{
		current = points[i];
		
		/* start with first element in triangulation */
		d_tri = triangulation;
		/* stores triangles which shall be deleted (i.e. replaced) */
		tmp_tri = NULL;
		num_tmp_tri = 0;
		skip_point = 0;
		
		/* iterated over all already existing triangles */
		do
		{
			/* complete == 0 indicates a triangle which is not finalized yet */
			if( d_tri->complete == 0 )
			{
				if ( (current.x == points[d_tri->v0].x && current.y == points[d_tri->v0].y) ||
						 (current.x == points[d_tri->v1].x && current.y == points[d_tri->v1].y) ||
						 (current.x == points[d_tri->v2].x && current.y == points[d_tri->v2].y) )
				{
					printf("inside... %f, %f\n", current.x, current.y);
					skip_point = 1;
					break;
				}
				
				Dx2 = (d_tri->center.x - current.x)*(d_tri->center.x - current.x);
				
				if( Dx2 >= d_tri->sqr_radius )
				{
					/* List of points is sorted, thus this triangle
				   * is not of interest anymore as its circumcenter is to far to
				   * the left to be of interest anymore
				   */
					d_tri->complete = 1;
				}
				else
				{
					/* triangle is still of interest */
					Dst2 = Dx2 + 
							(d_tri->center.y - current.y)*(d_tri->center.y - current.y);
					
					/* true, if point is inside circumcircle
					 * which means the triangle has to be replaced */
					if ( Dst2 < d_tri->sqr_radius ) 
					{	
						d_tri->complete = -1; /* flag as obsolete */
						++num_tmp_tri;
						
						/* store obsolete triangle */
						if ( tmp_tri == NULL ) /* if it is the first triangle to delete */
						{
							tmp_tri = create_triangle(d_tri->v0, d_tri->v1, d_tri->v2,
																			  NULL, NULL, points);
						}
						else /* all others */
						{
							Triangle2* t_tri = create_triangle(d_tri->v0, d_tri->v1, d_tri->v2,
																							tmp_tri, tmp_tri->next, points);
							if( tmp_tri->next )
								tmp_tri->next->prev = t_tri;
							tmp_tri->next = t_tri;						
						}	
							
					} /* (Dst2 < d_tri->sqr_radius) */
				} /* (Dx2 >= d_tri->sqr_radius) */
			} /* (d_tri->complete == 0) */
			
			d_tri = d_tri->next;
		} while ( d_tri != NULL );
		
		if( skip_point == 1 )
			continue;

		if( tmp_tri == NULL )
		{
			printf("\ttmp_tri is NULL for point %d\twith coords (%f,%f)\n", i, current.x, current.y );
			continue;
		}

		/* Create boundary list
		 * this is a list of all edges of all triangles which
		 * are obsolete
		 */
		Edge2* bd_list = malloc(sizeof(Edge2)*(num_tmp_tri*3));
		int counter=0;
		if( tmp_tri->next )
			do
			{
				bd_list[3*counter+0].v0 = MIN(tmp_tri->v0, tmp_tri->v1);
				bd_list[3*counter+0].v1 = MAX(tmp_tri->v0, tmp_tri->v1);
				
				bd_list[3*counter+1].v0 = MIN(tmp_tri->v1, tmp_tri->v2);
				bd_list[3*counter+1].v1 = MAX(tmp_tri->v1, tmp_tri->v2);
				
				bd_list[3*counter+2].v0 = MIN(tmp_tri->v2, tmp_tri->v0);
				bd_list[3*counter+2].v1 = MAX(tmp_tri->v2, tmp_tri->v0);
				
				++counter;
				tmp_tri = tmp_tri->next;
			} while(tmp_tri->next != NULL);
		
		bd_list[3*counter+0].v0 = MIN(tmp_tri->v0, tmp_tri->v1);
		bd_list[3*counter+0].v1 = MAX(tmp_tri->v0, tmp_tri->v1);
		
		bd_list[3*counter+1].v0 = MIN(tmp_tri->v1, tmp_tri->v2);
		bd_list[3*counter+1].v1 = MAX(tmp_tri->v1, tmp_tri->v2);
		
		bd_list[3*counter+2].v0 = MIN(tmp_tri->v2, tmp_tri->v0);
		bd_list[3*counter+2].v1 = MAX(tmp_tri->v2, tmp_tri->v0);
		
		/* free obsolete triangles */
		if( tmp_tri->prev )
			do
			{
				tmp_tri = tmp_tri->prev;
				free(tmp_tri->next);
			} while( tmp_tri->prev );
		free(tmp_tri);
		
		/* Use only those edges which occur once
		 * as all edges which occur twice are inner edges
		 * and thus not suitable
		 */
		Edge2 current_edge;
		char twice; /* twice == 1 means it appears twice */
		unsigned int k,j;
		for( k=0; k < num_tmp_tri*3; ++k )
		{
			twice = 0;
			current_edge = bd_list[k];
			for( j=0; j < num_tmp_tri*3; ++j )
				if( k != j && current_edge.v0 == bd_list[j].v0
									 && current_edge.v1 == bd_list[j].v1 )
					twice = 1;
			
			if(twice == 1)
				continue;

			/* add new triangle to triangulation
			 * i.e. the current point is connected with
			 * the points of each edge
			 */
			Triangle2* newTri = create_triangle( current_edge.v0,
																					 current_edge.v1,
																					 i /*current point index*/,
																					 triangulation, triangulation->next, points );

			if( triangulation->next)
				triangulation->next->prev = newTri;
			triangulation->next = newTri;
		}
		
		/* free edge list */
		free(bd_list);
	}


	/* after all computations are done, prepare the result */
	
	/* find the first triangle in our current triangulation
	 * that is not flagged as obsolete and start the final
	 * triangulation with it - as long as it is not connected
	 * with the super-triangle
	 */
	while(triangulation->complete == -1 ||
				triangulation->v0 >= NUM_P||
				triangulation->v1 >= NUM_P||
				triangulation->v2 >= NUM_P )
		triangulation = triangulation->next;
		
	Triangle2* final_triangulation = create_triangle( triangulation->v0,
		triangulation->v1, triangulation->v2, NULL, NULL, points );
	triangulation = triangulation->next;
	
	/* add all triangles which are not obsolete */
	do
	{
		if( triangulation->complete != -1 &&
				triangulation->v0 < NUM_P &&
				triangulation->v1 < NUM_P &&
				triangulation->v2 < NUM_P )
		{
			Triangle2* tmp =  create_triangle( triangulation->v0,
							triangulation->v1, triangulation->v2,
							final_triangulation, final_triangulation->next, points );
			if ( final_triangulation->next )
				final_triangulation->next->prev = tmp;
			final_triangulation->next = tmp;
		}
		
		triangulation = triangulation->next;
	} while( triangulation->next );
	
	if( triangulation->complete != -1 &&
				triangulation->v0 < NUM_P &&
				triangulation->v1 < NUM_P &&
				triangulation->v2 < NUM_P )
	{
		Triangle2* tmp =  create_triangle( triangulation->v0,
							triangulation->v1, triangulation->v2,
							final_triangulation, final_triangulation->next, points );
		if ( final_triangulation->next )
			final_triangulation->next->prev = tmp;
		final_triangulation->next = tmp;
	}
	
	/* delete all previous triangles */
	do
	{
		triangulation = triangulation->prev;
		free(triangulation->next);
	} while( triangulation->prev );
		
	/* return final triangulation */
	return final_triangulation;
}

void export_delaunay(Triangle2* triangulation, Point2* points)
{
	Triangle2* local_tri = triangulation;
	
	FILE* output = fopen("delaunay_out.eps", "w");
	
	fprintf(output, "%%!PS-Adobe-2.0 EPSF-1.2\n%%%%BoundingBox: -10 -10 %d %d\n",
					SIZE*10+10, SIZE*10+10);
	fprintf(output, "1 setlinejoin\n0 setlinewidth\n");
	do
	{
		fprintf(output, "%f %f moveto\n%f %f lineto stroke\n",
						points[local_tri->v0].x*10,points[local_tri->v0].y*10,
						points[local_tri->v1].x*10,points[local_tri->v1].y*10);
		fprintf(output, "%f %f moveto\n%f %f lineto stroke\n",
						points[local_tri->v1].x*10,points[local_tri->v1].y*10,
						points[local_tri->v2].x*10,points[local_tri->v2].y*10);
		fprintf(output, "%f %f moveto\n%f %f lineto stroke\n",
						points[local_tri->v2].x*10,points[local_tri->v2].y*10,
						points[local_tri->v0].x*10,points[local_tri->v0].y*10);
						
		local_tri = local_tri->next;
	} while( local_tri != NULL );

	/* draw circumcircles */
	/*local_tri = triangulation;
	fprintf(output, "0.5 setlinewidth\n");
	fprintf(output, "0.5 setgray\n");
	do
	{
		fprintf(output, "%f %f %f 0 360 arc closepath stroke\n",
							10*local_tri->center.x, 10*local_tri->center.y,
							10*sqrt(local_tri->sqr_radius) );
							
		local_tri = local_tri->next;
	} while( local_tri != NULL );*/
					
	fclose(output); 
}

BoundingBox2D findBounds(Point2D* points);
void init_supertriangle_points(BoundingBox2D* bbox, Point2D* points);
int reallocateEdgeArray(EdgeArray* arr, unsigned int size)
{
	if(arr->array != NULL)
		free(arr->array);
	
	arr->array = (Edge2D*)malloc(size);
	if(arr->array == NULL)
		return -1;
	arr->length = size;
	return 0;
}

void swapTriangles(Triangle2D* first, Triangle2D* second)
{
	Triangle2D tri = *first;
	*first = *second;
	*second = tri;
}

int moveObsoleteToBack(Triangle2D* arr, int firstObsolete, int firstMinusOne)
{
	int ret = firstMinusOne - 1;
	for(int i = firstObsolete; i < ret; ++i)
	{
		if(arr[i].complete == -1)
		{
			for(int j = ret; ret > i; --j)
			{
				if(arr[j].complete != -1)
				{
					swapTriangles(&arr[i], &arr[j]);
					ret = j - 1;
					break;
				}
			}
		}
	}
	return ret;
}

void delaunay2d(Point2D* points, Array* arr)
{
	BoundingBox2D bbox = findBounds(points);
	
	init_supertriangle_points(&bbox, points);
	
	set_triangle2d(NUM_P+0, NUM_P+1, NUM_P+2, 0, points, arr->array);
	
	
	double Dx2;
	double Dst2;
	unsigned int num_obsolete_tri;
	int firstObsolete;
// 	EdgeArray edgeArr = { 0, NULL };
	EdgeArray edgeArr;
	edgeArr.array = malloc(sizeof(Edge2D) * 3 * 2 * NUM_P);
	edgeArr.length = 3 * 2 * NUM_P;
	
	/* iterate over all points as one point at a time is inserted
	 * into the delaunay-triangulation such that it is a 
	 * delaunay-triangulation again
	 */
	int indexArray[NUM_P * 2 + 1];
	
	Triangle2D* tri;
	Point2D current;
	int skip_point;
	for(int i = 0; i < NUM_P; ++i)
	{
		current = points[i];
		
		skip_point = 0;
		num_obsolete_tri = 0;
		
		for(int j = 0; j < 2*NUM_P + 1; ++j)
		{
			tri = &arr->array[j];
			if(tri->complete == 0)
			{
				if((current.x == points[tri->v0].x && current.y == points[tri->v0].y)
					|| (current.x == points[tri->v1].x && current.y == points[tri->v1].y)
					|| (current.x == points[tri->v2].x && current.y == points[tri->v2].y))
				{
					printf("inside... %f, %f\n", current.x, current.y);
					skip_point = 1;
					break;
				}
				
				Dx2 = (tri->center.x - current.x) * (tri->center.x - current.x);
				if(Dx2 >= tri->sqr_radius)
					tri->complete = 1;
				else
				{
					Dst2 = Dx2 + (tri->center.y - current.y) * (tri->center.y - current.y);
					
					if(Dst2 < tri->sqr_radius)
					{
						tri->complete = -1;
						indexArray[num_obsolete_tri] = j;
						++num_obsolete_tri;
					}
				}
				
				
			}
		}
		
		
		if(skip_point)
			continue;
		
		if(num_obsolete_tri == 0)
			continue;
		
		Triangle2D* tri;
		for(int n = 0; n < num_obsolete_tri; ++n)
		{
			tri = &arr->array[indexArray[n]];
			
			edgeArr.array[3*n+0].v0 = MIN(tri->v0, tri->v1);
			edgeArr.array[3*n+0].v1 = MAX(tri->v0, tri->v1);
				
			edgeArr.array[3*n+1].v0 = MIN(tri->v1, tri->v2);
			edgeArr.array[3*n+1].v1 = MAX(tri->v1, tri->v2);
				
			edgeArr.array[3*n+2].v0 = MIN(tri->v2, tri->v0);
			edgeArr.array[3*n+2].v1 = MAX(tri->v2, tri->v0);
		}
		
		
		
		Edge2D current_edge;
		int twice; /* twice == 1 means it appears twice */
		
		for(unsigned int k = 0; k < num_obsolete_tri * 3; ++k)
		{
			twice = 0;
			current_edge = edgeArr.array[k];
			for(unsigned int j = 0; j < num_obsolete_tri * 3; ++j)
				if(k != j && current_edge.v0 == edgeArr.array[j].v0 && current_edge.v1 == edgeArr.array[j].v1)
				{
					twice = 1;
					break;
				}
			
			if(twice == 1)
				continue;

			/* add new triangle to triangulation
			 * i.e. the current point is connected with
			 * the points of each edge
			 */
			for(int n = 0; n < 2 * NUM_P + 1; ++n)
			{
				if(arr->array[n].complete == -1)
				{
					set_triangle2d(current_edge.v0, current_edge.v1, i, 0, points, &arr->array[n]);
					break;
				}
			}
		}
	}
}

// points are sorted, check only y
BoundingBox2D findBounds(Point2D* points)
{
	BoundingBox2D bbox = { .min = points[0], .max = points[0] };
	
	//TODO copy points[i] into local Point2D
	for(int i=1; i < NUM_P; ++i)
	{
		bbox.min.x = fmin(bbox.min.x, points[i].x);
		bbox.min.y = fmin(bbox.min.y, points[i].y);
		bbox.max.x = fmax(bbox.max.x, points[i].x);
		bbox.max.y = fmax(bbox.max.y, points[i].y);
	}
	return bbox;
}

//TODO test with pass-by-value
void init_supertriangle_points(BoundingBox2D* bbox, Point2D* points)
{
	//TODO replace fmax with inlined (MACRO) function
	double x_c = (bbox->min.x + bbox->max.x) / 2.;
	double y_c = (bbox->min.y + bbox->max.y) / 2.;
	double d_max = 3*fmax(bbox->max.x - bbox->min.x, bbox->max.y - bbox->min.y);
	Point2D p0 = init_point2d( x_c - 1*d_max, y_c - 0.75*d_max );
	Point2D p1 = init_point2d( x_c + 1*d_max, y_c - 0.75*d_max );
	Point2D p2 = init_point2d( x_c , y_c + 1.25*d_max );
	
	/* points for super-triangle are appended to point cloud list */
	points[NUM_P+0] = p0;
	points[NUM_P+1] = p1;
	points[NUM_P+2] = p2;
}

void export_delaunay2d(Array arr, Point2D* points)
{
	Triangle2D* local_tri;
	
	FILE* output = fopen("delaunay_out.eps", "w");
	
	fprintf(output, "%%!PS-Adobe-2.0 EPSF-1.2\n%%%%BoundingBox: -10 -10 %d %d\n",
					SIZE*10+10, SIZE*10+10);
	fprintf(output, "1 setlinejoin\n0 setlinewidth\n");
	printf("Calling export_delaunay2d with arr.firstMinusOne == %d\n", arr.firstMinusOne);
	for(int i = 1; i < NUM_P * 2 + 1; ++i)
	{
		local_tri = &arr.array[i];
		if(local_tri->complete == -1 || local_tri->v0 >= NUM_P || local_tri->v1 >= NUM_P || local_tri->v2 >= NUM_P)
			continue;
		fprintf(output, "%f %f moveto\n%f %f lineto stroke\n",
						points[local_tri->v0].x*10,points[local_tri->v0].y*10,
						points[local_tri->v1].x*10,points[local_tri->v1].y*10);
		fprintf(output, "%f %f moveto\n%f %f lineto stroke\n",
						points[local_tri->v1].x*10,points[local_tri->v1].y*10,
						points[local_tri->v2].x*10,points[local_tri->v2].y*10);
		fprintf(output, "%f %f moveto\n%f %f lineto stroke\n",
						points[local_tri->v2].x*10,points[local_tri->v2].y*10,
						points[local_tri->v0].x*10,points[local_tri->v0].y*10);
	}
					
	fclose(output); 
}
