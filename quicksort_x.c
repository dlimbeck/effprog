
#include "main.h"

void quicksortX_recursive(int start, int end, Point2* points)
{
	if( start < end )
	{
		int pivot = (start + end)/2;
		int newPivot = partitionX(start, end, pivot, points);
		
		quicksortX_recursive( start,      newPivot-1, points);
		quicksortX_recursive( newPivot+1, end,        points);
	}
}

int partitionX(int start, int end, int pivot, Point2* points)
{
	int i;
	int idx = start;
	Point2 pivotPt = points[pivot];
	
	swap( pivot, end, points );
	for ( i=start; i < end; ++i )
		if( points[i].x <= pivotPt.x )
		{
			swap( i, idx, points );
			++idx;
		}
	swap( idx, end, points );
	
	return idx;
}

void swap(int left, int right, Point2* points)
{
	Point2 temp = points[left];
	points[left] = points[right];
	points[right] = temp;
}

void quicksortX_recursive2d(int start, int end, Point2D* points)
{
	if( start < end )
	{
		int pivot = (start + end)/2;
		int newPivot = partitionX2d(start, end, pivot, points);
		
		quicksortX_recursive2d( start,      newPivot-1, points);
		quicksortX_recursive2d( newPivot+1, end,        points);
	}
}

int partitionX2d(int start, int end, int pivot, Point2D* points)
{
	int i;
	int idx = start;
	Point2D pivotPt = points[pivot];
	
	swap2d( pivot, end, points );
	for ( i=start; i < end; ++i )
		if( points[i].x <= pivotPt.x )
		{
			swap2d( i, idx, points );
			++idx;
		}
	swap2d( idx, end, points );
	
	return idx;
}

void swap2d(int left, int right, Point2D* points)
{
	Point2D temp = points[left];
	points[left] = points[right];
	points[right] = temp;
}
