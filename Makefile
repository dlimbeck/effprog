CC		= gcc
CFLAGS		= -std=c99 -g -p -lm
ARRAY_CFLAGS	= -DARRAY -std=c99 -g -p -lm

# %.o: %.c main.h
# 	$(CC) -c -o $@ $< $(CFLAGS)

# Delaunay: main.o delaunay.o quicksort_x.o
# 	$(CC) -o Delaunay main.o delaunay.o quicksort_x.o $(CFLAGS)

Delaunay:
	$(CC) -o Delaunay main.c delaunay.c quicksort_x.c $(CFLAGS)

Array:
	$(CC) -o Delaunay main.c delaunay.c quicksort_x.c $(ARRAY_CFLAGS)

